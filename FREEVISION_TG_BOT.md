# FairCoin FreeVision Bot

## Usage

### Currency conversion
```
/c[bid|mid|ask] <amount> [m|µ]<currency_shortcode> to [m|µ]<currency_shortcode>

examples:
/c 10 FAIR to EUR
/c 10 FAIR to mFAIR
/c 10 USD to FAIR
/c 100 FAIR to mBTC

show complete conversion table:
/c 1 FAIR

use the bid price for calculation ( defaulf exchange price)
/c ...
/cbid ...

use price between bid and ask for calculation
/cmid ...

use ask price for calculation
/cask ...
```

### Exchange

#### SignUp workflow
##### I. invite new telegram user
```
/invite_user <your token> <username of new user>

example:
/invite_user X....... telegramusername
```

##### II. create new account by invitation token
```
/new_account <registration token>

/new_account R.....
```

##### III. activate new account
```
/activate <user_token>

example:
/activate X.......
```

#### Order/Transaction workflow

##### 1. create order
```
/create_order <user_token> [buy|sell] <slots> <foreign_currency>

example:
/create_order X....... buy 1 EUR
```

##### 2. assign connector ( connector = peer that want do the exchange with you )
```
/assign_connector <user_token> <order_id> <connector_username>

example:
/assign_connector X....... A..... telegramusername
```

##### 3. connector confirm ( connector confirms that he / she want do the exchange )
```
/connector_confirm <connector user_token> <order_id> <creator`s username>

example:
/connector_confirm X....... A..... telegramusername
```

##### 4. creator has paid ( if the connector received the payment he / she set it as paid )
```
/creator_has_paid <connector user_token> <order_id> <creator username>

example:
/creator_has_paid X....... A..... telegramusername
```

##### 5. connector has paid ( if the creator of order received the payment from connector then he / she can set it as paid and the transaction is successfully processed )
```
/connector_has_paid <creator user_token> <order_id>

example:
/connector_has_paid X....... A.....+
```
