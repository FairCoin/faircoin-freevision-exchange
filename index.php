<?php
include_once('functions/freevision_ex_lang.php');
include_once('functions/db_connect.php');
$db=new db;

define('MAX_SLOTS',10);
define('SLOT_SIZE_FAIR',1000);

define('USER',$_GET['user']);
define('TOKEN',$_GET['token']);

define(EXCHANGE,Array(
  2 => 'buy',
  1 => 'sell'
  )
);

$notSignedIn='d-none';
$notActive='disabled';
//#### user sign-in
if(!empty(USER) && !empty(TOKEN)){
  $user_id=md5(USER.TOKEN);
  $sql = 'SELECT * FROM user WHERE AES_DECRYPT(id,KEY)="'.$user_id.'"';
  $result = $db->query($sql);
  if($row = $result->fetch_assoc() ){
    $notSignedIn='';
    if($row['active'] == 1) $notActive='';
  }
}

function notSignedIn(){
  return 't';
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>FairCoin FreeVision P2P Exchange
    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">
    <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet">
    <style>
    @font-face { font-family: 'MonospaceTypewriter';
             src: url('assets/css/MonospaceTypewriter.ttf') format('truetype'); }
    body {
      font-family: 'MonospaceTypewriter';
      background:lavender;
    }

    .container-fluid > .row:nth-child(3){
      border-bottom:1px solid grey;
      padding-bottom:0.1em;
    }

    .form-row {
      background:lavender;
    }

    .row.orderlist {
      margin:0.2rem;
      background:white;
    }

    .row.orderlist > .col {
      padding:0.5rem;

    }

    .container-fluid {
      border:0.5rem solid lavender;
      padding-left:0px;
      padding-right:0px;
    }

    .stage button {
      border:1px solid black;
    }

    .party {
      color:blue;
    }

    </style>
  </head>
  <body>
    <div class="container-fluid">
      <div class="form-row">
        <div class="form-group col">
          <button id="list_my_orders" class="actions btn btn-sm btn-outline-primary">sign-in</button>
          <button id="create_order" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" <?= $notActive ?>><b>1</b> create_order</button>
          <button id="assign_connector" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" disabled><b>2</b> assign_connector</button>
          <button id="connector_confirm" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" disabled><b>3</b> connector_confirm</button>
          <button id="creator_has_paid" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" disabled><b>4</b> creator_has_paid</button>
          <button id="connector_has_paid" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" disabled><b>5</b> connector_has_paid</button>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col">
          <button id="invite_user" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" <?= $notActive ?>><b>I</b> invite_user</button>
          <button id="new_account" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>" <?= $notActive ?>><b>II</b> new_account</button>
          <button id="activate" class="actions btn btn-sm btn-outline-primary <?= $notSignedIn ?>"><b>III</b> activate</button>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-5">
          Identification:
          <div class="input-group input-group-sm">
            <input id="username" type="text" placeholder="" class="params form-control" disabled>
            <input id="token" type="text" placeholder="" class="params form-control" disabled>
            <input id="orderId" type="text" placeholder="" class="params form-control" disabled>
            <div class="input-group-append">
              <button id="submit" class="params btn btn-sm btn-secondary" disabled>submit</button>
            </div>
          </div>
        </div>
        <div class="form-group col col-2 <?= $notSignedIn ?>">Exchange:
          <div class="input-group input-group-sm" align="center">
            <div class="input-group-prepend">
              <button id="exchangeBuy" class="params btn btn-sm btn-secondary" disabled><?= getLN('BUY') ?></button>
            </div>
            <div class="input-group-append">
              <button id="exchangeSell" class="params btn btn-sm btn-secondary" disabled><?= getLN('SELL') ?></button>
            </div>
          </div>
        </div>
        <div class="form-group col <?= $notSignedIn ?>">Ordervolume:
          <div class="input-group input-group-sm">
            <select class="params custom-select form-control-sm" id="slots" disabled>
            </select>
          </div>
        </div>
        <div class="form-group col <?= $notSignedIn ?>">Currency:
          <div class="input-group input-group-sm">
            <select class="params custom-select form-control-sm" id="currency" disabled></select>
          </div>
        </div>
      </div>
      <div class="form-row <?= $notSignedIn ?>">
        <div class="form-group col">
          <div class="input-group input-group-sm">
            <input id="botCommand" type="text" class="form-control" placeholder="telegram bot command ( send it to bot by PM )">
            <div class="input-group-append">
              <button id="botCommandCopy" class="btn btn-sm btn-secondary">copy</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row orderlist">
        <div class="col">
          <?php include_once('functions/exchange.php'); ?>
        </div>
      </div>
    </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="assets/js/json.js"></script>

<script>

var TICKER;

var toggle_actions=function(obj){ $('.actions').toggleClass('btn-outline-primary',true); $('.actions').toggleClass('btn-primary',false); $(obj).toggleClass('btn-primary',true); $(obj).toggleClass('btn-outline-primary',false); }
var disable_all_params=function(){ $('.params').attr('disabled',true); reset_valid_params('.is-valid'); reset_invalid_params('.is-invalid'); bot_command([]); reset_exchangeBtns(); $('.params').val(''); $('.params').attr('placeholder',''); }
var disable_params=function(a){ $(a).attr('disabled',true); $(a).attr('placeholder',''); }
var enable_params =function(a){ $(a).attr('disabled',false); }
var valid_params=function(a){ $(a).toggleClass('is-valid',true); }
var reset_valid_params=function(a){ $(a).toggleClass('is-valid',false); }
var invalid_params=function(a){ $(a).toggleClass('is-invalid',true); }
var reset_invalid_params=function(a){ $(a).toggleClass('is-invalid',false); }
var getAction=function(){ var A=$('.actions.btn-primary'); if( A.length == 1 ){ return A[0].id; } else { return ''; } }
var reset_exchangeBtns=function(){ $('#exchangeBuy,#exchangeSell').toggleClass('btn-info',false); $('#exchangeBuy,#exchangeSell').toggleClass('btn-secondary',true); }
var set_exchangeBtn=function(obj){ $(obj).toggleClass('btn-info',true); $(obj).toggleClass('btn-secondary',false); }
var bot_command=function(A){ $('#botCommand').val( A.join(' ') ); }
var bot_command_copy=function(){ $('#botCommand').select(); document.execCommand('copy'); }
var check_username=function(){
  var user=$('#username').val();
  reset_invalid_params('#username');
  if( user != '' ) return user;
  invalid_params('#username');
  return false;
}

var check_token=function(){
  var token=$('#token').val();
  reset_invalid_params('#token');
  if( token.match(/^X[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]$/g) != null ) return token;
  invalid_params('#token');
  return false;
}

var check_rtoken=function(){
  var token=$('#token').val();
  reset_invalid_params('#token');
  if( token.match(/^R[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]$/g) != null ) return token;
  invalid_params('#token');
  return false;
}

var check_orderId=function(){
  var orderId=$('#orderId').val();
  reset_invalid_params('#orderId');
  if( orderId.match(/^A[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]$/g) != null ) return orderId;
  invalid_params('#orderId');
  return false;
}

$( document ).ready(
  function() {

    $('.assign_connector').click(
      function(){
        $('#assign_connector').click();
        $('#orderId').val($(this).parent().parent().parent().parent()[0].cells[1].innerText);
        $('#token').val('<?= TOKEN ?>');
        valid_params('#token,#orderId');
      }
    );

    $('.connector_confirm').click(
      function(){
        $('#connector_confirm').click();
        $('#orderId').val($(this).parent().parent().parent().parent()[0].cells[1].innerText);
        $('#token').val('<?= TOKEN ?>');
        valid_params('#token,#orderId');
      }
    );

    $('.creator_has_paid').click(
      function(){
        $('#creator_has_paid').click();
        $('#orderId').val($(this).parent().parent().parent().parent()[0].cells[1].innerText);
        $('#token').val('<?= TOKEN ?>');
        valid_params('#token,#orderId');
      }
    );

    $('.connector_has_paid').click(
      function(){
        $('#connector_has_paid').click();
        $('#orderId').val($(this).parent().parent().parent()[0].cells[1].innerText);
        $('#token').val('<?= TOKEN ?>');
        valid_params('#token,#orderId');
      }
    );

    $('#list_my_orders').click(
      function(){
        disable_all_params();
        enable_params('#username,#token,#submit');
        $('#username').attr('placeholder','my username');
        $('#token').attr('placeholder','my token');
        toggle_actions(this);
      }
    );

    $('#create_order').click(
      function(){
        var C=content_load('data/ticker','json');
        var tmp='<option></option>';
        $.each(C,
          function(i,v){
            tmp+='<option value="'+i+'">'+i+'</option>';
          }
        );
        disable_all_params();
        $('#currency').html(tmp);
        enable_params('#currency');
        toggle_actions(this);
        $('#token').val('<?= TOKEN ?>');
        valid_params('#token');
      }
    );

    $('#invite_user').click(
      function(){
        disable_all_params();
        enable_params('#username,#token,#submit');
        $('#username').attr('placeholder','username of new user');
        $('#token').attr('placeholder','creator`s token');
        $('#token').val('<?= TOKEN ?>');
        toggle_actions(this);
      }
    );

    $('#new_account').click(
      function(){
        disable_all_params();
        enable_params('#token,#submit');
        $('#token').attr('placeholder','registration token');
        toggle_actions(this);
      }
    );

    $('#activate').click(
      function(){
        disable_all_params();
        enable_params('#token,#submit');
        $('#token').attr('placeholder','user`s token');
        $('#token').val('<?= TOKEN ?>');
        toggle_actions(this);
      }
    );

    $('#assign_connector').click(
      function(){
        disable_all_params();
        enable_params('#username,#token,#orderId,#submit');
        $('#username').attr('placeholder','connector`s username');
        $('#token').attr('placeholder','creator`s token');
        $('#orderId').attr('placeholder','order id');
        toggle_actions(this);
      }
    );

    $('#connector_confirm').click(
      function(){
        disable_all_params();
        enable_params('#username,#token,#orderId,#submit');
        $('#token').attr('placeholder','connector`s token');
        $('#orderId').attr('placeholder','order id');
        $('#username').attr('placeholder','creator`s username');
        toggle_actions(this);
      }
    );

    $('#creator_has_paid').click(
      function(){
        disable_all_params();
        enable_params('#username,#token,#orderId,#submit');
        $('#token').attr('placeholder','connector`s token');
        $('#orderId').attr('placeholder','order id');
        $('#username').attr('placeholder','creator`s username');
        toggle_actions(this);
      }
    );

    $('#connector_has_paid').click(
      function(){
        disable_all_params();
        enable_params('#token,#orderId,#submit');
        $('#token').attr('placeholder','creator`s token');
        $('#orderId').attr('placeholder','order id');
        toggle_actions(this);
      }
    );

    $('#currency').change(
      function(){
        if( $(this).val() != '' ){
          enable_params('#exchangeBuy,#exchangeSell');
          valid_params('#currency');
          reset_valid_params('#slots,#exchangeBuy,#exchangeSell');
          disable_params('#slots,#token,#submit');
          reset_exchangeBtns();
        } else {
          disable_params('#exchangeBuy,#exchangeSell,#slots');
          reset_valid_params('#currency,#slots,#exchangeBuy,#exchangeSell');
          reset_exchangeBtns();
        }
      }
    );

    $('#exchangeBuy').click(
      function(){
        reset_exchangeBtns();
        set_exchangeBtn( this );
        TICKER=content_load('data/ticker_ask','json');
        get_slotvolumes();
        enable_params('#slots');
        valid_params('#exchangeBuy,#exchangeSell');
        disable_params('#token,#submit');
        reset_valid_params('#slots,#username');
      }
    );

    $('#exchangeSell').click(
      function(){
        reset_exchangeBtns();
        set_exchangeBtn( this );
        TICKER=content_load('data/ticker','json');
        get_slotvolumes();
        enable_params('#slots');
        valid_params('#exchangeBuy,#exchangeSell');
        disable_params('#token,#submit');
        reset_valid_params('#slots,#username');
      }
    );

    $('#slots').change(
      function(){
        if( $(this).val() != '' ){
          enable_params('#token,#submit');
          $('#token').attr('placeholder','my token');
          valid_params('#slots');
        } else {
          disable_params('#token,#submit');
          $('#token').attr('placeholder','');
          reset_valid_params('#slots');
        }
      }
    );

    $('#submit').click( function(){ subm() } );
    $('#botCommandCopy').click( function(){ bot_command_copy() } );
  }
);

function subm(){
  var action=getAction();
  var user=$('#username').val();
  var token=$('#token').val();
  var orderId=$('#orderId').val();

  switch(getAction()){
    case 'list_my_orders':
      if( !check_username() || !check_token() ){
        return;
      } else {
        reset_invalid_params('.params');
        valid_params('#username,#token');
        window.location.href = '?user=' + check_username() + '&token=' + check_token();
      }
    break;

    case 'create_order':
      if( !check_token() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var exchange=$('#exchangeBuy').hasClass('btn-info');
        var A=[];
        A.push('/create_order');
        A.push(check_token());
        A.push((exchange) ? 'buy' : 'sell');
        A.push($('#slots').val());
        A.push($('#currency').val());
        bot_command(A);
        valid_params('#token,#botCommand');
      }
    break;

    case 'invite_user':
      if( !check_username() || !check_token() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/invite_user');
        A.push(check_token());
        A.push(check_username());
        bot_command(A);
        valid_params('#username,#token,#botCommand');
      }
    break;

    case 'new_account':
      if( !check_rtoken() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/new_account');
        A.push(check_rtoken());
        bot_command(A);
        valid_params('#token,#botCommand');
      }
    break;

    case 'activate':
      if( !check_token() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/activate');
        A.push(check_token());
        bot_command(A);
        valid_params('#token,#botCommand');
      }
    break;

    case 'assign_connector':
      if( !check_username() || !check_token() || !check_orderId() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/assign_connector');
        A.push(check_token());
        A.push(check_orderId());
        A.push(check_username());
        bot_command(A);
        valid_params('#username,#token,#orderId,#botCommand');
      }
    break;

    case 'connector_confirm':
      if( !check_username() || !check_token() || !check_orderId() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/connector_confirm');
        A.push(check_token());
        A.push(check_orderId());
        A.push(check_username());
        bot_command(A);
        valid_params('#username,#token,#orderId,#botCommand');
      }
    break;

    case 'creator_has_paid':
      if( !check_username() || !check_token() || !check_orderId() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/creator_has_paid');
        A.push(check_token());
        A.push(check_orderId());
        A.push(check_username());
        bot_command(A);
        valid_params('#username,#token,#orderId,#botCommand');
      }
    break;

    case 'connector_has_paid':
      if( !check_token() || !check_orderId() ){
        bot_command([]);
        reset_valid_params('#botCommand');
        return;
      } else {
        var A=[];
        A.push('/connector_has_paid');
        A.push(check_token());
        A.push(check_orderId());
        bot_command(A);
        valid_params('#token,#orderId,#botCommand');
      }
    break;

  }
}

function get_slotvolumes(){
  var tmp='<option></option>';
  var slotvolume=TICKER[$('#currency').val()].last * <?= SLOT_SIZE_FAIR ?>;
  for(var i=1;i<=<?= MAX_SLOTS ?>;i++){
    var volume=Math.round((i*slotvolume)*100000000)/100000000;
    tmp+='<option value="'+i+'">'+volume+'</option>';
  }
  $('#slots').html(tmp);
}


</script>

  </body>
</html>
