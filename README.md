# FairCoin FreeVision Exchange

## The exchange system

We change OtherCurrencies to FairCoin to free market price but FairCoin to OtherCurrencies only to support price.

Example:
If the market price is 0.20€ we change FairCoin to Other to 0.10€ but Other to FairCoin to 0.20€

So arbitrage trading can prevented and in opposite the service itself do arbitrage trading and the win of aribtrage trading will go to the existing FairCoin system and supports the official price of 1.20€ or we invest the win into technical improvements of FairCoin where all FairCoiner will profit.

Our exchange system will be open and transparent for everyone ( with respect to privacy to the members ) and processed by p2p exchanges made by authorized supporters.

### The slot system

To make the exchanges efficient to handle we uses a slot system to organize all exchanges.

Every slot has same size for example 1000 FairCoin. So we would support only exchanges in 100€ steps.

The maximum number of slots is limited to 10 at the moment to consider the challenges of P2P exchange. So it is also possible for smaller FairCoiner to process the exchanges and we can reach a good level of decentralization easier.

Because of the mechanism that an arbitrage trading should not be possible the exchanges doesn't contain risks for the connectors that do the exchange.

To prevent that arbitrage traders would do the exchanges the freevision exchange will define trustful members in first stage and later a web-of-trust mechanism will implemented.
