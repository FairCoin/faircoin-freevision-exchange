//https://www.coinexchange.io/api/v1/getmarketsummary?market_id=200
//https://www.coinexchange.io/api/v1/getorderbook?market_id=200
var DATA;
var PAIR;
var DEFAULT_CURRENCY='eur';
var GRAPH;

$( document ).ready(
  function() {
    $( '#curr' ).change(function() {
      get_data( $(this).val() );
    });
    get_data( (( getQueryVariable('c') == false ) ? DEFAULT_CURRENCY : getQueryVariable('c')  ) );
    $('#resetZoom').click(function(){ unzoomGraph(); });
  }


);
var lastlegend='..loading..';

function legendFormatter(data) {
  if (data.x == null) {
    // This happens when there's no selection and {legend: 'always'} is set.
    //return '<br>' + data.series.map(function(series) { return series.dashHTML + ' ' + series.labelHTML }).join('<br>');
    return lastlegend;
  }

  var html = this.getLabels()[0] + ': ' + data.xHTML;
  data.series.forEach(function(series) {
    if (!series.isVisible) return;
    var labeledData = series.labelHTML + ': ' + series.yHTML;
    if (series.isHighlighted) {
      labeledData = '<b>' + labeledData + '</b>';
    }
    html += '<br>' + series.dashHTML + ' ' + labeledData;
  });
  lastlegend=html;
  return html;
}

function unzoomGraph() {
  GRAPH.updateOptions({
    dateWindow: null,
    valueRange: null
  });
}

function create_graph(){

  var D=[];
  var L=['time','fv_price','fm_price'];
  var factor_curr=0;
  var factor=0;

  DATA.history.slice(1).forEach(
    function(v,i){
      var A=v.split(/,/g);
      if( A.length > 1 ){
        factor=parseFloat(A[5]);
        if( factor_curr == 0 ) factor_curr=factor;
        fact=factor/factor_curr;
        var LMH_fv=[A[1]*fact,A[1]*fact,A[2]*fact];
        var LMH_fm=[A[3]*fact,(A[3]*fact+A[4]*fact)/2,A[4]*fact];
        D.push([new Date(A[0]*1000),LMH_fv,LMH_fm]);
      }
    }
  );

  console.log(D);

  GRAPH = new Dygraph(
    document.getElementById("pricegraph"),
    D, // path to CSV file
    {
      customBars:true,
      labels:L,
      labelsSeparateLines: true,
      labelsKMB: true,
      legendFormatter: legendFormatter,
      labelsDiv: document.getElementById('legend'),
      legend:'always',
      hideOverlayOnMouseOut:false,
      series: {
              'fv_price': { axis: 'y' },
              'fm_price': { axis: 'y' }
            },
      axes : {
        y : {
          valueFormatter: function(v) {
            return v.toFixed(4) + ' ' + DATA.config.FACTOR_PREPOSITION + DATA.config.CURRENCY.toUpperCase();
          },
          axisLabelFormatter: function(v) {
            return v.toFixed(3);
          }
        }
      }
    }          // options
  );

  var linear = document.getElementById("linear");
  var log = document.getElementById("log");
  var setLog = function(val) {
    GRAPH.updateOptions({ logscale: val });
    linear.disabled = !val;
    log.disabled = val;
  };
  linear.onclick = function() { setLog(false); };
  log.onclick = function() { setLog(true); };
  GRAPH.setSelection(GRAPH.file_.length-1);

}

function get_data(curr){

  DATA=content_load('functions/get_data.php?c=' + curr,'json' );
  PAIR=content_load('data/coingecko_currencies.json');
  $('#curr').html('');
  var tmp='';
  PAIR.sort().forEach(
    function(v,i){
      tmp+='<option value="'+v+'" '+((v==DATA.config.CURRENCY) ? 'selected' : '' )+'>'+v.toUpperCase()+'</option>';
    }
  );
  $('#curr').html(tmp);
  $('#fv_bid').html( DATA.fv_bid.toString().slice(0,-2) + '<sub>' + DATA.fv_bid.toString().slice(-2) + '</sub>' );
  $('#fm_bid').html( DATA.fm_bid.toString().slice(0,-2) + '<sub>' + DATA.fm_bid.toString().slice(-2) + '</sub>' );
  $('#fm_ask').html( DATA.fm_ask.toString().slice(0,-2) + '<sub>' + DATA.fm_ask.toString().slice(-2) + '</sub>' );
  $('#fv_ask').html( DATA.fv_ask.toString().slice(0,-2) + '<sub>' + DATA.fv_ask.toString().slice(-2) + '</sub>' );
  $('.curr').html(DATA.config.FACTOR_PREPOSITION + DATA.config.CURRENCY.toUpperCase() + ' / FAIR' );
  $('#conf').html( JSON.stringify( DATA.config).replace(/\,/g,'<br>').replace(/\"|\{|\}/g,'').replace(/\:/g,' = ') );
  console.log(curr);
  $('.api.share > a').attr('href',window.location.origin + window.location.pathname+'?c='+curr);
  $('.api.data > a').attr('href','data/freevision_'+DATA.config.CURRENCY+'.json');
  $('.api.history > a').attr('href','data/freevisionHistory_'+DATA.config.CURRENCY+'.csv');
  var tmp='';
  DATA.history.slice(0,1).concat(DATA.history.slice(1).reverse()).forEach(
    function(v,i){
      tmp+=history_out(v,i);
    }
  );
  $('#history').html(tmp);
  create_graph();
}

function history_out(v,i){
  var V=v.split(/,/g);
  var tmp='';
  if( V.length > 1 ){
    tmp+='<div class="row">'
    tmp+='<div class="col-sm-2">'+(( i == 0 ) ? V[0] : new Date(V[0]*1000 - new Date().getTimezoneOffset()*60*1000).toJSON().slice(0,16).replace(/T/g,' '))+'</div>';
    tmp+='<div class="col">'+V[1]+'</div>';
    tmp+='<div class="col">'+V[3]+'</div>';
    tmp+='<div class="col">'+V[4]+'</div>';
    tmp+='<div class="col">'+V[2]+'</div>';
    tmp+='<div class="col">'+V[5]+'</div>';
    //tmp+='<div class="col">'+V[6]+'</div>';
    tmp+='</div>';
  }
  return tmp;
}
