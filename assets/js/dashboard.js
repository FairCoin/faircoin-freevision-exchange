//https://www.coinexchange.io/api/v1/getmarketsummary?market_id=200
//https://www.coinexchange.io/api/v1/getorderbook?market_id=200
var DATA;
var GRAPH;

$( document ).ready(
  function() {
    get_data();
    $('#resetZoom').click(function(){ unzoomGraph(); });
  }

);
var lastlegend='..loading..';

function legendFormatter(data) {
  if (data.x == null) {
    // This happens when there's no selection and {legend: 'always'} is set.
    //return '<br>' + data.series.map(function(series) { return series.dashHTML + ' ' + series.labelHTML }).join('<br>');
    return lastlegend;
  }

  var html = this.getLabels()[0] + ': ' + data.xHTML;
  data.series.forEach(function(series) {
    if (!series.isVisible) return;
    var labeledData = series.labelHTML + ': ' + series.yHTML;
    if (series.isHighlighted) {
      labeledData = '<b>' + labeledData + '</b>';
    }
    html += '<br>' + series.dashHTML + ' ' + labeledData;
  });
  lastlegend=html;
  return html;
}

function unzoomGraph() {
  GRAPH.updateOptions({
    dateWindow: null,
    valueRange: null
  });
}

function create_graph(){

  var D=[];
  var L=['time','discussion','merchants','supporters','exchange'];

  D=DATA.slice(1);

  GRAPH = new Dygraph(
    document.getElementById("graph1"),
    D
    , // path to CSV file
    {
      labels:L
    }          // options
  );

}

function get_data(){

  DATA=content_load('data/stats/telegram.csv');
  DATA.forEach(
    function(v,i){
      DATA[i][0]=new Date(v[0]);
      DATA[i][1]=v[1]*1;
      DATA[i][2]=v[2]*1;
      DATA[i][3]=v[3]*1;
      DATA[i][4]=v[4]*1;
    }
  )
  var L=DATA[DATA.length-1];
  $('#telegrammembers').html(L[1] + '|' + L[2] + '|' + L[3] + '|' + L[4]);
  create_graph();
}
