<?php

include_once('tg_conf.php');

function tg_bot($param){

  $url=BOT_API.$param;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $data = curl_exec($ch);
  curl_close($ch);
  return json_decode( $data, true );

}

function read_data($fn){
  $fp=fopen($fn,'r');
  $data=fread($fp,filesize($fp));
  fclose($fp);
  return $data;
}

function write_data($fn,$data,$mode){
  $fp=fopen($fn,$mode);
  fwrite($fp,$data);
  fclose($fp);
}

function get_members_count($chat_id){
  return intVal( tg_bot('getChatMembersCount?chat_id='.$chat_id )['result']);
}

function get_members_counts(){

  $timestamp=date('Y-m-d',time());
  $tmp=$timestamp;

  if( filemtime(CHAT_DATA_URL) + UPDATE_INTERVAL < time() ) {

    foreach(CHAT as $chat){
      $membersCount=0;
      $membersCount=get_members_count($chat['chat_id']);
      if( $membersCount > 0 ){
        $tmp.=','.( $membersCount-$chat['bot_offset'] );
      }
    }
    $tmp.='
';
    write_data(CHAT_DATA_URL, $tmp, 'a+');
  }

}

get_members_counts();

?>
