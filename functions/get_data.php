<?php
ini_set('display_errors',1);
//define('COINEXCHANGE','../data/coinexchange.json');
//define('COINEXCHANGE_URL','https://www.coinexchange.io/api/v1/getmarketsummary?market_id=200');
define('COINEXCHANGE','../data/coinexchange.json');
define('COINEXCHANGE_URL','https://www.coinexchange.io/api/v1/getorderbook?market_id=200');
//https://www.coinexchange.io/api/v1/getorderbook?market_id=200
//define('BITCOINAVERAGE','../data/bitcoinaverage.json');
//define('BITCOINAVERAGE_URL','https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCEUR');

define('FREEVISION', '../data/freevision_');
define('FREEVISION_HISTORY', '../data/freevisionHistory_');

define('COINGECKO_CURRENCIES', '../data/coingecko_currencies.json');
define('COINGECKO_CURRENCIES_URL','https://api.coingecko.com/api/v3/simple/supported_vs_currencies');

define('COINGECKO_FAIR_PAIRS', '../data/coingecko_fair_pairs.json');
define('COINGECKO_FAIR_PAIRS_URL','https://api.coingecko.com/api/v3/simple/price?ids=faircoin&vs_currencies=');

define('WALLET_TICKER_BID', '../data/ticker.json');
define('WALLET_TICKER_ASK', '../data/ticker_ask.json');
define('WALLET_TICKER_MID', '../data/ticker_mid.json');
define('WALLET_TICKER_FREEMARKET', '../data/ticker_freemarket.json');

// electrumfair history rates
define('HISTORY_ELECTRUMFAIR_BID', '../data/history_');
define('HISTORY_ELECTRUMFAIR_MID', '../data/history_mid_');
define('HISTORY_ELECTRUMFAIR_FREEMARKET', '../data/history_freemarket_');
define('HISTORY_ELECTRUMFAIR_FAIRCOOP', '../data/history_faircoop_');
define('FAIRCOOP_PRICE_EUR',1.20);
define('RESET_HISTORY_RATES', false);

define('DEFAULT_CURRENCY','eur');

define('ASK_DEPTH_SLOTS',10);
define('BID_DEPTH_SLOTS',10);
define('SLOT_SIZE_FAIR',1000);

define('ASK_OFFSET_PERC',1.05);
define('BID_OFFSET_PERC',0.95);

define('REFRESH_TIME_SECONDS',3600);

define('FACTOR_PREPOSITION',Array(
    1 => '',
    0.001=> 'm',
    0.000001=>'µ'
  )
);

define('LOG_NORMALIZED_25',
  Array(
    1,
    1.25,
    1.5,
    1.75,
    2,
    2.5,
    3,
    3.5,
    4,
    5,
    6,
    7,
    8
  )
);

define('LOG_NORMALIZED_5',
  Array(
    1,
    1.05,
    1.1,
    1.15,
    1.2,
    1.25,
    1.3,
    1.35,
    1.4,
    1.5,
    1.6,
    1.7,
    1.8,
    1.9,
    2,
    2.1,
    2.2,
    2.3,
    2.4,
    2.5,
    2.6,
    2.7,
    2.8,
    3,
    3.25,
    3.5,
    3.75,
    4,
    4.25,
    4.5,
    4.75,
    5,
    5.25,
    5.5,
    6,
    6.5,
    7,
    7.5,
    8,
    8.5,
    9,
    9.5
    )
  );


function curl($url){

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $data = curl_exec($ch);
  curl_close($ch);

  return $data;

}

function update_data(){

  $ARR=[COINEXCHANGE,COINGECKO_CURRENCIES];
  $ARR_URL=[COINEXCHANGE_URL,COINGECKO_CURRENCIES_URL];
  $f=false;

  foreach( $ARR as $key => $value ){
    if( file_exists($value) ){
      if( filemtime($value) + REFRESH_TIME_SECONDS > time() ){
      } else {
        $f=true;
      }
    } else {
      $f=true;
    }


    if( $f ){
      $json=curl($ARR_URL[$key]);
      $J=json_decode($json,true);
      if( !is_null( $J ) ){
        $fp=fopen($value,'w+');
        fwrite($fp,$json);
        fclose($fp);
      }
      if($value == COINGECKO_CURRENCIES ){
        $CURR=json_decode($json,true);
        $urlparam='';
        foreach($CURR as $currency){
          $urlparam.=$currency.'%2C';
        }
        $json=curl(COINGECKO_FAIR_PAIRS_URL.$urlparam);
        $J=json_decode($json,true);
        if( !is_null( $J ) ){
          $fp=fopen(COINGECKO_FAIR_PAIRS,'w+');
          fwrite($fp,$json);
          fclose($fp);
        }
      }
    }
  }

  $curr=empty($_GET['c']) ? DEFAULT_CURRENCY : $_GET['c'];

  return get_data($f,$curr);
}

function get_data($upd,$curr){

  if( !$upd ){
    // load data from folder
    $ARR=[FREEVISION.$curr.'.json',FREEVISION_HISTORY.$curr.'.csv'];
    $ARR_ID=['FREEVISION','HISTORY'];
    $JS=[];
    foreach( $ARR as $key => $value ){
      $fp=fopen($value,'r');
      $json=fread($fp,filesize($value));
      fclose($fp);
      if( preg_match('/\.csv/',$value ) ){
        $JS[$ARR_ID[$key]]=$json;
      } else {
        $JS[$ARR_ID[$key]]=json_decode($json,true);
      }
    }
    $H=preg_split('/
/',$JS['HISTORY']);

    $JS['FREEVISION']['history']=$H;
    return json_encode($JS['FREEVISION']);
  }

  /*
  ###################################################
    get orderbook of coinexchange
  ---------------------------------------------------
  */

  // recalculate data
  $ARR=[COINEXCHANGE,COINGECKO_FAIR_PAIRS];
  $ARR_ID=['BTCFAIR','COINGECKO_FAIR_PAIRS'];
  $JS=[];
  foreach( $ARR as $key => $value ){
    $fp=fopen($value,'r');
    $json=fread($fp,filesize($value));
    fclose($fp);
    $JS[$ARR_ID[$key]]=json_decode($json,true);
  }

  // exchange pairs
  $PAIR=$JS['COINGECKO_FAIR_PAIRS']['faircoin'];

  // coinexchange orderbook data
  $COINEXCHANGE_FAIR_BTC=$JS['BTCFAIR']['result'];

  // timestamp of entry
  $timestamp=time();

  // initialize currency ticker for wallets
  $WALLET_TICKER=[];
  $WALLET_TICKER_ASK=[];
  $WALLET_TICKER_MID=[];
  $WALLET_TICKER_FREEMARKET=[];

  // iterate all available currency pairs
  foreach($PAIR as $currency=>$price){

    // filenames
    $fn_freevision=FREEVISION.$currency.'.json';
    $fn_freevision_history=FREEVISION_HISTORY.$currency.'.csv';

    // load existing price data
    if( file_exists($fn_freevision) ){
      $fp=fopen($fn_freevision,'r');
      $json=fread($fp,filesize($fn_freevision));
      fclose($fp);
    } else {
      $json=json_encode([
          'timestamp'=>0,
          'fv_bid'=>0,
          'fv_ask'=>0,
          'fm_bid'=>0,
          'fm_ask'=>0,
          'config'=>[
            'CURRENCY'=>$currency,
            'FACTOR'=>1,
            'FACTOR_PREPOSITION'=>FACTOR_PREPOSITION[1],
            'ASK_DEPTH'=>0,
            'BID_DEPTH'=>0,
            'ASK_OFFSET_PERC'=>0,
            'BID_OFFSET_PERC'=>0
          ]
        ]);
    }
    $FREEVISION=json_decode($json,true);

    /* calculate normalized prices */

    // currency multiplier to btc prices
    $m=$price/$PAIR['btc'];

    // calculate normalization factor to 1
    $f=false;
    $factor=1;
    while(!$f){
      if( $price/$factor < 0.001 ){
        $factor/=1000;
      } else {
        $f=true;
      }
    }


    $fv_bid_current=floatval($FREEVISION['fv_bid']*$FREEVISION['config']['FACTOR']/$factor);

    /* check market max depth bid */
    $depth_fair=0; $depth_price_btc=0;
    foreach($COINEXCHANGE_FAIR_BTC['BuyOrders'] as $order ){
      if( $depth_fair < (BID_DEPTH_SLOTS * SLOT_SIZE_FAIR) ){
        if( $FREEVISION['fv_bid'] <= $m*$order['Price']/$factor ){
          $depth_fair+=$order['Quantity'];
          $depth_price_btc=$order['Price'];
        } else {
          if( $depth_price_btc == 0 ){
            $depth_price_btc=$order['Price'];
          }
          $depth_fair=100;
          break;
        }
      } else {
        $depth_fair=BID_DEPTH_SLOTS * SLOT_SIZE_FAIR;
        //$depth_price_btc=$order['Price'];
        break;
      }
    }
    $bid_depth_fair = intVal( $depth_fair / SLOT_SIZE_FAIR ) * SLOT_SIZE_FAIR;
    $bid_depth_slots= intVal( $depth_fair / SLOT_SIZE_FAIR );
    $bid_price_btc=$depth_price_btc;

    /* check market max depth ask */
    $depth_fair=0; $depth_price_btc=0;
    foreach($COINEXCHANGE_FAIR_BTC['SellOrders'] as $order ){
      if( $depth_fair < (ASK_DEPTH_SLOTS * SLOT_SIZE_FAIR) ){
        if( $FREEVISION['fv_ask'] >= $m*$order['Price']/$factor ){
          $depth_fair+=$order['Quantity'];
          $depth_price_btc=$order['Price'];
        } else {
          if( $depth_price_btc == 0 ){
            $depth_price_btc=$order['Price'];
          }
          break;
        }
      } else {
        $depth_fair=ASK_DEPTH_SLOTS * SLOT_SIZE_FAIR;
        //$depth_price_btc=$order['Price'];
        break;
      }
    }
    $ask_depth_fair = intVal( $depth_fair / SLOT_SIZE_FAIR ) * SLOT_SIZE_FAIR;
    $ask_depth_slots= intVal( $depth_fair / SLOT_SIZE_FAIR );
    $ask_price_btc=$depth_price_btc;

    $fv_bid_bid = freevision_price_normalize(25,$m * $bid_price_btc,BID_OFFSET_PERC,'bid')/$factor;
    $fv_bid_ask = freevision_price_normalize(25,$m * $ask_price_btc,BID_OFFSET_PERC,'bid')/$factor;
    $fv_ask = freevision_price_normalize(5,$m*$ask_price_btc,ASK_OFFSET_PERC,'ask')/$factor;



    /*
    add hysteresis for freevision bid price
    if new calculation is lower than current price then ask side counts
    if new calculation is higher than current price then bid side counts
    */

    if( $fv_bid_ask < $fv_bid_current ){
      $fv_bid=$fv_bid_ask;
    } elseif( $fv_bid_bid > $fv_bid_current ) {
      $fv_bid=$fv_bid_bid;
    } else {
      $fv_bid=$fv_bid_current;
    }

    // adjust prices
    $FREEVISION['timestamp']=$timestamp;
    $FREEVISION['fv_bid']=number_format( $fv_bid, 4, '.', '' );
    $FREEVISION['fv_ask']=number_format( $fv_ask, 4, '.', '');
    $FREEVISION['fm_bid']=number_format( $m*$bid_price_btc/$factor, 4, '.', '' );
    $FREEVISION['fm_ask']=number_format( $m*$ask_price_btc/$factor, 4, '.', '' );
    $FREEVISION['config']['FACTOR']=$factor;
    $FREEVISION['config']['FACTOR_PREPOSITION']=FACTOR_PREPOSITION[$factor];
    $FREEVISION['config']['BID_DEPTH']=number_format( $fv_bid * $bid_depth_fair, 0, '.', '' );
    $FREEVISION['config']['BID_DEPTH_SLOTS']=$bid_depth_slots;
    $FREEVISION['config']['ASK_DEPTH']=number_format( $fv_ask * $ask_depth_fair, 0, '.', '' );
    $FREEVISION['config']['ASK_DEPTH_SLOTS']=$ask_depth_slots;
    $FREEVISION['config']['BID_OFFSET_PERC']=BID_OFFSET_PERC;
    $FREEVISION['config']['ASK_OFFSET_PERC']=ASK_OFFSET_PERC;

    // add freevision prices to wallet ticker
    $WALLET_TICKER_BID[strtoupper($currency)]['last']=round( $FREEVISION['fv_bid']*$factor , 8 );
    $WALLET_TICKER_ASK[strtoupper($currency)]['last']=round( $FREEVISION['fv_ask']*$factor , 8 );
    $WALLET_TICKER_MID[strtoupper($currency)]['last']=round( ( $FREEVISION['fv_bid']+$FREEVISION['fv_ask'] ) / 2 * $factor, 8 );

    // add freemarket price to wallet ticker
    $WALLET_TICKER_FREEMARKET[strtoupper($currency)]['last']=round( ( $FREEVISION['fm_bid']+$FREEVISION['fm_ask'] ) / 2 * $factor, 8 );

    // overwrite current prices with new prices
    $fp=fopen($fn_freevision,'w+');
    fwrite($fp,json_encode($FREEVISION));
    fclose($fp);

    // create history entry
    $hist=$timestamp.','.
      $FREEVISION['fv_bid'].','.
      $FREEVISION['fv_ask'].','.
      $FREEVISION['fm_bid'].','.
      $FREEVISION['fm_ask'].','.
      $FREEVISION['config']['FACTOR'].','.
      $FREEVISION['config']['BID_DEPTH'].','.
      $FREEVISION['config']['ASK_DEPTH'].'
';

    // create history entry for electrumfair wallets

    // create new history file with header if not exists
    if( !file_exists($fn_freevision_history) ){
      $head='timestamp,'.
        'fv_bid,'.
        'fv_ask,'.
        'fm_bid,'.
        'fm_ask,'.
        'factor,'.
        'bid_depth,'.
        'ask_depth
';
      $fp=fopen($fn_freevision_history,'w+');
      fwrite($fp,$head);
      fclose($fp);
    }

    // append history entry to (.csv)
    $fp=fopen($fn_freevision_history,'a+');
    fwrite($fp,$hist);
    fclose($fp);

    add_history_rates_entry(HISTORY_ELECTRUMFAIR_BID,$currency,$timestamp,$FREEVISION['fv_bid'],RESET_HISTORY_RATES);
    add_history_rates_entry(HISTORY_ELECTRUMFAIR_MID,$currency,$timestamp,($FREEVISION['fv_bid']+$FREEVISION['fv_ask'])/2,RESET_HISTORY_RATES );
    add_history_rates_entry(HISTORY_ELECTRUMFAIR_FREEMARKET,$currency,$timestamp,$WALLET_TICKER_FREEMARKET[strtoupper($currency)]['last'],RESET_HISTORY_RATES );
    add_history_rates_entry(HISTORY_ELECTRUMFAIR_FAIRCOOP,$currency,$timestamp,FAIRCOOP_PRICE*$PAIR[$currency]/$PAIR['eur'],false );

  }

  // update wallet ticker file (.json)
  $fp=fopen(WALLET_TICKER_BID,'w+');
  fwrite($fp,json_encode($WALLET_TICKER_BID));
  fclose($fp);

  $fp=fopen(WALLET_TICKER_MID,'w+');
  fwrite($fp,json_encode($WALLET_TICKER_MID));
  fclose($fp);

  $fp=fopen(WALLET_TICKER_ASK,'w+');
  fwrite($fp,json_encode($WALLET_TICKER_ASK));
  fclose($fp);

  $fp=fopen(WALLET_TICKER_FREEMARKET,'w+');
  fwrite($fp,json_encode($WALLET_TICKER_FREEMARKET));
  fclose($fp);

  return get_data(false,$curr);

}

function add_history_rates_entry($fn,$currency,$timestamp,$price,$reset){

  $fn.=strtoupper($currency).'.json';

  if($reset){
    ### get prices from freevision history files
    $f=FREEVISION_HISTORY.$currency.'.csv';

    $fp=fopen($f,'r');
    $c=fread($fp,filesize($f));
    fclose($fp);

    $A=preg_split('/
/',$c);

    array_shift($A);

    $JS=Array();

    foreach($A as $a){
      $B=preg_split('/,/',$a);
      if( $B[0] != '' ) $JS[date('Y-m-d', $B[0])] = $B[1]*1;
    }

  } else {
    ### apppend new entry

    if( file_exists($fn) ){
      $fp=fopen($fn,'r');
      $json=fread($fp,filesize($fn));
      fclose($fp);
    } else {
      $json='{}';
    }

    $JS=json_decode($json,true);
    $JS[date('Y-m-d')] = $price*1;

  }

  $fp=fopen($fn,'w+');
  fwrite($fp,json_encode($JS));
  fclose($fp);

}

function freevision_price_normalize($norm, $bid,$offset_perc,$orientation){

  if( $norm == 25 ){
    $NORMALIZED=LOG_NORMALIZED_25;
  } elseif( $norm == 5 ){
    $NORMALIZED=LOG_NORMALIZED_5;
  } else { return 0; }

  $a = $offset_perc * $bid;

  for($i=-8;$i<9;$i++){
    if( $a <= pow(10,$i) ) break;
  }
  $p=$i-1;
  $e=pow(10, $p);

  for($d=0;$d<count($NORMALIZED);$d++){
    if( $a < $e*$NORMALIZED[$d] ) break;
  }

  if( $orientation == 'bid' ) $d--;
  if( $d == count($NORMALIZED) ){
    $d=0; $e*=10;
  }

  $f=$e*$NORMALIZED[$d];

  return $f;

}

// test mode => custom function calls
if( !empty($_GET['test']) ){
  if( $_GET['test'] == 1 ){
    //echo freevision_price_normalize(25, 0.8341,BID_OFFSET_PERC,'bid');
    exit;
  }
}


echo update_data();

//### update telegram stats
include_once('tg.php');


?>
