<?php

include_once('db_connect.php');
include_once('freevision_ex_lang.php');
include_once('token.php');

define('EXCHANGE',Array(
  'buy' => 2,
  'sell' => 1
  )
);

define('SLOT_SIZE_FAIR',1000);
define('OTT_EXPIRATION_SECONDS',3600);

function invite_user( $user, $token, $usern ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/invite_user <your token> <username of new user>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/invite_user X....... telegramusername```';
    return $msg;
  }

  if( empty($user) or empty($token) or empty($usern) ) return false;

  $db=new db;

  $user_id=md5($user.$token);

  // sign-up user
  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1) return $signin;

  remove_expired_ott();

  $ott='R'.getToken(5);
  $usern=md5($usern);
  $expiration=time()+OTT_EXPIRATION_SECONDS;

  $sql = 'INSERT INTO OTT (token, user_id, username, expiration ) VALUES ("'.$ott.'", AES_ENCRYPT("'.$user_id.'",KEY),AES_ENCRYPT("'.$usern.'",KEY),'.$expiration.')';

  if ($db->query($sql) === TRUE) {
      // success

      return getLN('NEW_TOKEN_FOR_INVITATION').' *'.$ott.'*

 '.getLN('SEND_IT_TO_NEW_USER');
  } else {
      // error return false
      $db=null;
      return false;
  }
}

function remove_expired_ott(){
  $db=new db;
  $expiration=time();
  $sql = 'DELETE FROM OTT WHERE expiration<'.$expiration;
  $db->query($sql);
  $db=null;
}

function new_account( $user, $ott ){

  if( $ott == 'help' ){
    $msg='```'.PHP_EOL.'/new_account <registration token>'.PHP_EOL.PHP_EOL.'/new_account R.....```';
    return $msg;
  }

  if( empty($user) or empty($ott) ) return false;

  $db=new db;

  remove_expired_ott();

  $usern=md5($user);

  if( get_user_count($db) > 0 ){
    // get sign-up token
    $sql = 'SELECT *,AES_DECRYPT(user_id,KEY) AS user__id FROM OTT WHERE token="'.$ott.'" AND AES_DECRYPT(username,KEY)="'.$usern.'"';
    $result = $db->query($sql);

    if( $row = $result->fetch_assoc()){
      $parent_id=$row['user__id'];
    } else {
      $db=null;
      return getLN('INVALID_TOKEN');
    }
  } else {
    $parent_id='';
  }

  $token='X'.getToken(7);
  $id=md5( $user.$token );
  $timestamp=time();
  $parent_id=($parent_id == '') ? $id : $parent_id;

  $sql = 'INSERT INTO user (timestamp, date, id, parent_id ) VALUES ('.$timestamp.', Now(),AES_ENCRYPT("'.$id.'",KEY),AES_ENCRYPT("'.$parent_id.'",KEY))';

  if ($db->query($sql) === TRUE) {
      // success

      // remove OTT
      $sql = 'DELETE FROM OTT WHERE token="'.$ott.'"';
      $db->query($sql);
      $db=null;

      return getLN('YOUR_NEW_TOKEN').' *'.$token.'*

 '.getLN('ACTIVATE_ACCOUNT');
  } else {
      // error return false
      $db=null;
      return false;
  }
}

function activate( $user, $token ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/activate <user_token>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/activate X.......```';
    return $msg;
  }

  if( empty($user) or empty($token) ) return false;

  $db=new db;

  $user_id=md5($user.$token);

  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1){
    $signin=check_sign_in($db,$user_id,0);
    if($signin!=1) return $signin;

    $sql = 'UPDATE user SET active=1 WHERE AES_DECRYPT(id,KEY)="'.$user_id.'" AND active=0';
    $db->query($sql);

    $rows= mysqli_affected_rows( $db->conn );

    if( $rows == 1 ){
        // activate new account
        $sql = 'UPDATE user SET active=1 WHERE AES_DECRYPT(id,KEY)="'.$user_id.'" AND active=0';
        return getLN('ACCOUNT_ACTIVATED_SUCCESSFULLY');
    } else {
        $sql = 'SELECT * FROM user WHERE AES_DECRYPT(id,KEY)="'.$user_id.'" AND active=1';
        $result = $db->query($sql);
        if( mysqli_num_rows($result) === 1 ){
          $db=null;
          return getLN('SIGNIN_SUCCESSFULLY');
        } else {
          $db=null;
          return getLN('SIGNIN_FAILED');
        }
        return false;
    }
  } else {
    return getLN('SIGNIN_SUCCESSFULLY');
  }

  /*
  $sql = 'SELECT * FROM user WHERE AES_DECRYPT(id,KEY)="'.$id.'"';
  $result = $db->query($sql);

  if( ! $row = $result->fetch_assoc()){
    $db=null;
    return getLN('SIGNIN_FAILED');
  }
  */

  // activate new account

  //$sql = "SELECT * FROM user WHERE id='".$id."' AND active=0";

}

function create_order( $user, $token, $exchange, $slots, $currency ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/create_order <user_token> [buy|sell] <slots> <foreign_currency>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/create_order X....... buy 1 EUR```';
    return $msg;
  }

  if( empty($user) or empty($token) or empty($exchange) or empty($slots) or empty($currency) ) return false;

  if( empty( EXCHANGE[$exchange] ) ) return getLN('SECOND_ARGUMENT_BUY_OR_SELL');
  if( ! ( is_numeric( $slots ) and $slots > 0 and $slots < 10 ) ) return getLN('THIRD_ARGUMENT_1_10');

  $TICKER=get_ticker($exchange);
  if( empty($TICKER[$currency]) ) return getLN('UNKNOWN_CURRENCY');

  $slotsize = SLOT_SIZE_FAIR;
  $price_limit=$TICKER[$currency]['last'];

  $db=new db;

  $user_id=md5($user.$token);

  // check signin
  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1) return $signin;

  $orderId='A'.getToken(5);

  $id=md5($user).$orderId;

  // create order
  $sql = 'INSERT INTO orders (id, user_id, timestamp, exchange, slots, currency, price_limit, slotsize_fair ) VALUES (AES_ENCRYPT("'.$id.'",KEY), AES_ENCRYPT("'.$user_id.'",KEY), '.time().','.EXCHANGE[$exchange].','.$slots.',"'.$currency.'",'.$price_limit.','.$slotsize.')';

  if ($db->query($sql) === TRUE) {
    return getLN('YOUR_ORDER_ID').'*'.$orderId.'*';
  } else {
    return getLN('ORDER_CREATION_FAILURE');
  }
}

function assign_connector( $user, $token, $orderId, $usern ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/assign_connector <user_token> <order_id> <connector_username>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/assign_connector X....... A..... telegramusername```';
    return $msg;
  }

  if( empty($user) or empty($token) or empty($orderId) or empty($usern) ) return false;

  $db=new db;

  $user_id=md5($user.$token);
  $conn_id=md5($usern);

  // check signin
  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1) return $signin;

  $id=md5($user).$orderId;

  $sql = 'UPDATE orders SET conn_id=AES_ENCRYPT("'.$conn_id.'",KEY), connector_confirmed=0 WHERE AES_DECRYPT(id,KEY)="'.$id.'"';
  $db->query($sql);
  $rows=mysqli_affected_rows( $db->conn );

  if ( $rows == 1 ) {
    return getLN('CONNECTOR_ASSIGNED_SUCCESSFULLY').'*'.$usern.'*';
  } else {
    return getLN('CONNECTOR_ASSIGNMENT_FAILURE').'*'.$usern.'*';
  }
}

function connector_confirm( $user, $token, $orderId, $usern ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/connector_confirm <connector user_token> <order_id> <creator`s username>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/connector_confirm X....... A..... telegramusername```';
    return $msg;
  }

  if( empty($user) or empty($token) or empty($orderId) or empty($usern) ) return false;

  $db=new db;

  $user_id=md5($user.$token);

  // check signin
  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1) return $signin;

  $id=md5( $usern ).$orderId;

  // create order

  $sql = 'UPDATE orders SET conn_id=AES_ENCRYPT("'.$user_id.'",KEY), connector_confirmed=1 WHERE AES_DECRYPT(id,KEY)="'.$id.'"';
  $db->query($sql);

  $rows=mysqli_affected_rows( $db->conn );

  if ( $rows == 1 ) {
    return getLN('CONNECTOR_CONFIRMED_SUCCESSFULLY');
  } else {
    return getLN('CONNECTOR_CONFIRMATION_FAILURE');
  }
}

function creator_has_paid( $user, $token, $orderId, $usern ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/creator_has_paid <connector user_token> <order_id> <creator username>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/creator_has_paid X....... A..... telegramusername```';
    return $msg;
  }

  if( empty($user) or empty($token) or empty($orderId) or empty($usern) ) return false;

  $usern=preg_replace('/^\@/','',$usern);

  $db=new db;

  $user_id=md5($user.$token);

  // check signin
  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1) return $signin;

  $id=md5( $usern ).$orderId;

  // create order
  $sql = 'UPDATE orders SET creator_paid=1 WHERE AES_DECRYPT(id,KEY)="'.$id.'"';
  $db->query($sql);

  $rows=mysqli_affected_rows( $db->conn );

  if ( $rows == 1 ) {
    return getLN('ACTION_DONE_SUCCESSFULLY');
  } else {
    return getLN('ACTION_FAILURE');
  }
}

function connector_has_paid( $user, $token, $orderId ){

  if( $token == 'help' ){
    $msg='```'.PHP_EOL.'/connector_has_paid <creator user_token> <order_id>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/connector_has_paid X....... A.....```';
    return $msg;
  }

  if( empty($user) or empty($token) or empty($orderId) ) return false;

  $db=new db;

  $user_id=md5($user.$token);

  // check signin
  $signin=check_sign_in($db,$user_id,1);
  if($signin!=1) return $signin;

  $id=md5( $user ).$orderId;

  $sql = 'UPDATE orders SET connector_paid=1 WHERE AES_DECRYPT(id,KEY)="'.$id.'"';
  $db->query($sql);
  $rows=mysqli_affected_rows( $db->conn );

  if ( $rows == 1 ) {
    return getLN('ACTION_DONE_SUCCESSFULLY');
  } else {
    return getLN('ACTION_FAILURE');
  }
}

function check_sign_in($db,$user_id,$active){
  $sql = 'SELECT * FROM user WHERE AES_DECRYPT(id,KEY)="'.$user_id.'" AND active='.$active;
  $result = $db->query($sql);

  if( $row = $result->fetch_assoc()){
    $db=null;
    return true;
  } else {
    return getLN('SIGNIN_FAILED');
  }
}

function get_user_count($db){
  $sql = 'SELECT COUNT(*) AS total FROM user';
  $result = $db->query($sql);

  if( $row = $result->fetch_assoc()){
    return $row['total'];
  }
}

function get_ticker($exchange){

  if( EXCHANGE[$exchange] == 2 ){
    $fn='../data/ticker_ask';
  } else {
    $fn='../data/ticker';
  }

  $fp=fopen($fn,'r');
  $json=fread($fp,filesize($fn));
  fclose($fp);
  return json_decode($json,true);
}

//echo new_account('TonyFord','RF7O7K');
?>
