<?php

ini_set('display_errors', 1);

include_once('tg_conf.php');
include_once('token.php');
include_once('db_connect.php');

include_once('freevision_ex_lang.php');
include_once('freevision_ex.php');

//define('BOT_API', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');

function processMessage($message) {
  // process incoming message
  global $LANG;

  $message_id = $message['message_id'];
  $chat_id = $message['chat']['id'];
  $LANG=$message['from']['language_code'];

  if (isset($message['text'])) {
    // incoming text message
    $text = $message['text'];

    $CMD=preg_split('/ /', preg_replace('/  /',' ',$text));

    if ( strpos($text, "/cbid") === 0  || $text == '/c' || strpos($text, "/c ") === 0 ) {

      $reply=command_c('ticker',(( strpos($text, "/cbid ") === 0 || $text != '/c' ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3],$CMD[4]);
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', "text" => $reply ));

    } else if ( strpos($text, "/cmid") === 0 ) {

      $reply=command_c('ticker_mid',(( strpos($text, "/cmid ") === 0 ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3],$CMD[4]);
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', "text" => $reply ));

    } else if ( strpos($text, "/cask") === 0 ) {

      $reply=command_c('ticker_ask',(( strpos($text, "/cask ") === 0 ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3],$CMD[4]);
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', "text" => $reply ));

    } else if ( strpos($text, "/invite_user") === 0 ) {

      $reply=command_invite_user($message,(( strpos($text, "/invite_user ") === 0 ) ? $CMD[1] : 'help'),$CMD[2]);
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/new_account") === 0 ) {

      $reply=command_new_account($message,(( strpos($text, "/new_account ") === 0 ) ? $CMD[1] : 'help'));
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/create_order") === 0 ) {

      $reply=command_create_order($message,(( strpos($text, "/create_order ") === 0 ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3],$CMD[4],$CMD[5]);
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/assign_connector") === 0 ) {

      $reply=command_assign_connector($message,(( strpos($text, "/assign_connector ") === 0 ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3] );
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/connector_confirm") === 0 ) {

      $reply=command_connector_confirm($message,(( strpos($text, "/connector_confirm ") === 0 ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3] );
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/creator_has_paid") === 0 ) {

      $reply=command_creator_has_paid($message,(( strpos($text, "/creator_has_paid ") === 0 ) ? $CMD[1] : 'help'),$CMD[2],$CMD[3] );
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/connector_has_paid") === 0 ) {

      $reply=command_connector_has_paid($message,(( strpos($text, "/connector_has_paid ") === 0 ) ? $CMD[1] : 'help'),$CMD[2]);
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if ( strpos($text, "/activate") === 0 ) {

      $reply=command_activate($message,(( strpos($text, "/activate ") === 0 ) ? $CMD[1] : 'help'));
      if( $reply ) apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'parse_mode' => 'markdown', 'text' => $reply ));

    } else if (strpos($text, "/help") === 0 ) {
      $reply='https://git.fairkom.net/FairCoin/faircoin-freevision-exchange/blob/master/FREEVISION_TG_BOT.md';
      apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => $reply ));
      // stop now
    } else {

      apiRequestWebhook("sendMessage", array('chat_id' => $chat_id, "reply_to_message_id" => $message_id, "text" => 'Cool'));
    }
  } else {
    $document=$message['document'];
    if($document['mime_type'] == 'image/png'){
      //$file=apiRequest("getFile", array('file_id' => $message['document']['file_id']));
      //apiRequest("sendPhoto", array('chat_id' => $chat_id, "photo" => $file));
      //apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'Test
      //'.$file['path']));
    }
  }
}


function command_c($fl,$amount,$in,$to,$out){

  if( $amount == 'help' ){
    $msg='```'.PHP_EOL.'/c[bid|mid|ask] <amount> [m|µ]<currency_shortcode> to [m|µ]<currency_shortcode>'.PHP_EOL.PHP_EOL.'example:'.PHP_EOL.'/c 100 FAIR to mBTC'.PHP_EOL.'/cbid 100 FAIR to mBTC'.PHP_EOL.'c == cbid'.PHP_EOL.'/cmid 100 FAIR to mBTC'.PHP_EOL.'/cask 100 FAIR to mBTC```';
    return $msg;
  }

  if( is_numeric( $amount ) ){
    if( !empty($in) ){
      $fn='../data/'.$fl;
      if( file_exists( $fn )){
        $fp=fopen($fn,'r');
        $PRICE=json_decode( fread($fp,filesize($fn)),true);
        fclose($fp);

        // check source currency prefix
        $prefix=substr( $in,0,2 );
        if( $prefix == 'µ' ){
          $currency=strtoupper( substr( $in,2,10) );
        } else {
          $prefix=substr( $in,0,1 );
          $currency=strtoupper( substr( $in,1,10) );
        }

        if( ( !empty( $PRICE[$currency] ) || $currency == 'FAIR' )  && ( $prefix == 'm' || $prefix == 'µ' ) ){
          switch($prefix){
            case 'm':
              $in_factor=1000;
              $in_prefix=$prefix;
            break;
            case 'µ':
              $in_factor=1000000;
              $in_prefix=$prefix;
            break;
          }
          $in_currency=$currency;
        } else {
          $in_factor=1;
          $in_currency=strtoupper( $in );
        }

        // check target currency prefix
        if( !empty($out) ){

          $prefix=substr( $out,0,2 );
          if( $prefix == 'µ' ){
            $currency=strtoupper( substr( $out,2,10) );
          } else {
            $prefix=substr( $out,0,1 );
            $currency=strtoupper( substr( $out,1,10) );
          }

          if( ( !empty( $PRICE[$currency] ) || $currency == 'FAIR' ) && ( $prefix == 'm' || $prefix == 'µ' ) ){
            switch($prefix){
              case 'm':
                $out_factor=1000;
                $out_prefix=$prefix;
              break;
              case 'µ':
                $out_factor=1000000;
                $out_prefix=$prefix;
              break;
            }
            $out_currency=$currency;
          } else {
            $out_factor=1;
            $out_currency=strtoupper( $out );
          }
        } else {
          $out_factor=1;
          $out_currency=NULL;
        }


        if( $in_currency == 'FAIR' && $to == 'to' && !empty( $out_currency ) ) {
          if( !empty( $PRICE[$out_currency] ) ){
            $price=$PRICE[$out_currency]['last']/$in_factor*$out_factor;
            $reply=$amount.' '.$in_prefix.$in_currency.' = '.round( $amount*$price , 8 ).' '.$out_prefix.$out_currency;
            $reply.=' ( '.$price*$out_factor/$in_factor.' '.$out_prefix.$out_currency.' / '.$in_prefix.'FAIR )';
          } else if( $out_currency == 'FAIR' ){
            $price=1/$in_factor*$out_factor;
            $reply=$amount.' '.$in_prefix.$in_currency.' = '.round( $amount*$price , 8 ).' '.$out_prefix.$out_currency;
            $reply.=' ( '.$out_factor/$in_factor.' '.$out_prefix.$out_currency.' / '.$in_prefix.'FAIR )';
          } else {
            $reply='currency not available!';
          }
        } else if( strtoupper( $in_currency ) == 'FAIR' ) {
          $reply='';
          foreach($PRICE as $key=>$value ){
            $price=$PRICE[$key]['last']/$in_factor*$out_factor;
            $reply.=$amount.' '.$in_prefix.$in_currency.' = '.round( $amount*$price , 8 ).' '.$out_prefix.$key;
            $reply.=' ( '.$price*$out_factor/$in_factor.' '.$out_prefix.$key.' / '.$in_prefix.'FAIR )'.PHP_EOL;
          }
        } else {
          $price=$PRICE[$in_currency]['last'];
          $reply=$amount.' '.$in_prefix.$in_currency.' = '.round( $amount/$price*$out_factor/$in_factor , 2 ).' '.$out_prefix.'FAIR';
          $reply.=' ( '.$price/$out_factor*$in_factor.' '.$in_prefix.$in_currency.' / '.$out_prefix.'FAIR )';
        }
        return $reply;
      }
    }
  }
}

function command_invite_user($message, $token, $user ){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');

  return invite_user( $message['from']['username'], trim($token), trim($user) );

}


function command_new_account($message, $ott){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');


  //apiRequestJson("sendMessage", array('chat_id' => $message['chat']['id'], "text" => json_encode($message['username']), 'reply_markup' => array(
  //    'hide_keyboard' => true) ) );

  return new_account( $message['from']['username'], trim($ott) );

  //return $message['from']['username'].'-'.$token;
  /*
  apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => json_encode($message['username']), 'reply_markup' => array(
    'keyboard' => array(array('Hello', 'Hi')),
    'one_time_keyboard' => true,
    'resize_keyboard' => true)));

  apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'Nice to meet you'));
  */
}

function command_activate($message, $token){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');
  return activate( $message['from']['username'], trim($token) );

}

function command_create_order($message, $token, $exchange, $min_slots, $max_slots, $currency ){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');
  return create_order( $message['from']['username'], trim($token),$exchange, $min_slots, $max_slots, $currency );

}

function command_assign_connector($message, $token, $orderId, $user ){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');
  return assign_connector( $message['from']['username'], trim($token),trim($orderId), $user );

}

function command_connector_confirm($message, $token, $orderId, $user ){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');
  return connector_confirm( $message['from']['username'], trim($token),trim($orderId), $user );

}

function command_creator_has_paid($message, $token, $orderId, $user ){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');
  return creator_has_paid( $message['from']['username'], trim($token),trim($orderId), $user );

}

function command_connector_has_paid($message, $token, $orderId ){

  if( $message['chat']['type'] != 'private' ) return getLN('ASK_ME_BY_PM');
  return connector_has_paid( $message['from']['username'], trim($token),trim($orderId) );

}



$content = file_get_contents("php://input");
$update = json_decode($content, true);

if (!$update) {
  // receive wrong update, must not happen
  exit;
}

if (isset($update["message"])) {
  processMessage($update["message"]);
} else if (isset($update["inline_query"])) {
    /*
    $inlineQuery = $update["inline_query"];
    $queryId = $inlineQuery["id"];
    $queryText = $inlineQuery["query"];

    if (isset($queryText) && $queryText !== "") {
      apiRequestJson("answerInlineQuery", [
        "inline_query_id" => $queryId,
        "results" => json_encode($inlineQuery),
        "cache_time" => 86400,
      ]);
    } else {
      apiRequestJson("answerInlineQuery", [
        "inline_query_id" => $queryId,
        "results" => [
          [
            "type" => "article",
            "id" => "0",
            "title" => "Unnikked Blog",
            "message_text" => json_encode($inlineQuery),
          ],
        ]
      ]);
    }
    */
}



?>
