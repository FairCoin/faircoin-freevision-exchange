<?php


if( !empty($_GET['help']) ){
  ?>
  <h4>Get it Started!</h4>
  <p>The FreeVision exchange system is a Peer-2-Peer exchange and processed by a telegram bot.<p>
  <p>Peer-2-Peer exchanges has the advantage of decentrality and independency but disadvantage of a high dependency to trust to unknown Peers.
  </p><p>To reduce the risk of fraud Peers can only use and join to the exchange by invitations of registered Peers.
  Peers that invite new Peers need to ensure that the new Peers are serious and trustful. Otherwise it could become to a bumerang if the invited users fraud other Peers.
</p><p>The graph of invitations will be public ( with secured privacy ). It can be called as a web-of-trust. The web-of-trust will be improved in next versions in the future and increases the security.
</p><p>Another aspect why we use invitations is that Peers that invites new Peers can/should instruct new Peers about the exchange and how it works. So nobody must feel alone and have a contact person from the beginning.
</p><p>All trades are done by your own risks. If you use the exchange then we can NOT guarantuee anything. It should be clear that the FreeVision exchange is still an experiment and we need to get experiences and improve it step-by-step.
</p><p>Now join the telegram group <a href="https://t.me/joinchat/BPPPUVXXo2bC-tGI1lolMw">here</a>
  </p>
  <?
  return;
}

if( !empty(USER) && !empty(TOKEN) ){
  $user_id=md5(USER.TOKEN);
  $usern=md5(USER);
  $sql='WHERE AES_DECRYPT(user_id,KEY)="'.$user_id.'" OR AES_DECRYPT(conn_id,KEY)="'.$user_id.'" OR AES_DECRYPT(conn_id,KEY)="'.$usern.'"';
}

// check signin
$sql = 'SELECT *,RIGHT(AES_DECRYPT(id,KEY),6) AS orderId, AES_DECRYPT(user_id,KEY) AS user__id FROM orders '.$sql.' ORDER BY timestamp DESC';
$result = $db->query($sql);

$JS=Array();
$tmp='<tr><th>Timestamp</th>'.((empty($user_id)) ? '' : '<th class="party">orderId</th><th class="party">Party</th>').'<th>Exchange</th><th>SlotSize(FAIR)</th><th>Slots</th><th>Price</th><th>Ordersize</th><th>Currency</th><th>Workflow</th></tr>';

while( $row = $result->fetch_assoc()){
  $ordersize=$row['price_limit'] * $row['slots'] * $row['slotsize_fair'];
  $tmp.='<tr>';
  $tmp.='<td>'.date( 'Y-m-d H:i', $row['timestamp'] ).'</td>';
  if( !empty($user_id) ){
    $tmp.='<td class="party">'.$row['orderId'].'</td>';
    $tmp.='<td class="party">'.(($user_id == $row['user__id']) ? getLN('CREATOR') : getLN('CONNECTOR') ).'</td>';
  }
  $tmp.='<td>'.EXCHANGE[$row['exchange']].'</td>';
  $tmp.='<td>'.$row['slotsize_fair'].'</td>';
  $tmp.='<td>'.$row['slots'].'</td>';
  $tmp.='<td>'.number_format($row['price_limit']*1,8).'</td>';
  $tmp.='<td>'.$ordersize.'</td>';
  $tmp.='<td>'.$row['currency'].'</td>';
  $tmp.='<td>
  <div class="input-group stage">
    <div class="input-group-prepend">
      <button class="btn btn-sm btn-success" title="create_order" disabled>1</button>
      <button class="assign_connector btn btn-sm btn-'.(($row['conn_id'] != '') ? 'success' : 'dark').'" title="assign_connector"'.(($row['connector_confirmed'] || $notSignedIn!='' || $user_id != $row['user__id'] ) ? ' disabled' : '').'>2</button>
      <button class="connector_confirm btn btn-sm btn-'.(($row['connector_confirmed']) ? 'success' : 'dark').'" title="connector_confirm"'.(($row['connector_confirmed'] || $row['conn_id'] == '' || $notSignedIn!='' || $user_id == $row['user__id'] ) ? ' disabled' : '').'>3</button>
      <button class="creator_has_paid btn btn-sm btn-'.(($row['creator_paid']) ? 'success' : 'dark').'" title="creator_has_paid"'.(($row['connector_confirmed']==0 || $row['creator_paid']==1 || $notSignedIn!='' || $user_id == $row['user__id'] ) ? ' disabled' : '').'>4</button>
    </div>
    <button class="connector_has_paid btn btn-sm btn-'.(($row['connector_paid']) ? 'success' : 'dark').'" title="connector_has_paid"'.(($row['creator_paid']==0 || $row['connector_paid']==1 || $notSignedIn!='' || $user_id != $row['user__id'] ) ? ' disabled' : '').'>5</button>
  </div>
  </td>';
  $tmp.='</tr>';
}

echo '<h2>FairCoin FreeVision P2P Exchange</h2>';
echo '<table class="table">'.$tmp.'</table>';


?>
