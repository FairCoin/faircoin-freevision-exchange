<?php

define('LN_default','en');

$LANG = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

$fn='../languages/lang_'.$LANG.'_exchange.json';
if( !file_exists($fn) ) $LANG=LN_default;
$fn='../languages/lang_'.$LANG.'_exchange.json';

$fp=fopen($fn,'r');
define('LN',json_decode(fread($fp,filesize($fn)),true));
fclose($fp);

?>
